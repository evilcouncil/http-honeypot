package main

import (
	//standard
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	//external
	"github.com/spf13/viper"
)

type Record struct {
	RemoteHost  string      `json:"remotehost"`
	Method      string      `json:"method"`
	Url         string      `json:"url"`
	Headers     http.Header `json:"headers"`
	UserAgent   string      `json:"useragent"`
	Referer     string      `json:"referer"`
	EventTime   uint64      `json:"eventtime"`
	PostValues  url.Values  `json:"postvalues"`
	Destination string      `json:"destination"`
	SensorName  string      `json:"sensorname"`
}

var (
	appLogger  *log.Logger
	port       string
	logPath    string
	sensorName string
)

func processRequest(r *http.Request) Record {
	data := Record{}
	data.SensorName = sensorName
	data.RemoteHost = r.RemoteAddr
	data.Method = r.Method
	data.Url = r.RequestURI
	data.Headers = r.Header
	data.UserAgent = r.UserAgent()
	data.Referer = r.Referer()
	data.EventTime = uint64(time.Now().Unix())
	data.Destination = r.Host
	r.ParseForm()
	data.PostValues = r.PostForm
	return data
}

func buildJSON(r Record) (string, error) {
	data, err := json.Marshal(r)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

func logRequest(r *http.Request) error {
	data := processRequest(r)
	dataJson, err := buildJSON(data)
	if err != nil {
		return err
	}
	appLogger.Println(dataJson)

	return nil
}

func loadConfigs() error {
	viper.SetConfigType("env")
	viper.SetConfigFile(".env")
	viper.AutomaticEnv()
	if err := viper.ReadInConfig(); err != nil {
		fmt.Println("Error reading .env file for settings.")
		_, newErr := os.Stat(".env")
		if newErr == nil {
			return err
		}
		fmt.Println(".env file does not exist, continuing.")
	}

	viper.SetDefault(PORT, DEFAULT_PORT)
	port = viper.GetString(PORT)
	viper.SetDefault(LOGFILE, DEFAULT_LOGFILE)
	logPath = viper.GetString(LOGFILE)
	viper.SetDefault(SENSOR_NAME, DEFAULT_SENSOR_NAME)
	sensorName = viper.GetString(SENSOR_NAME)
	return nil
}

func handleIndex(w http.ResponseWriter, r *http.Request) {
	if err := logRequest(r); err != nil {
		log.Fatal(err)
	}
	if r.RequestURI == "/_ping" {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(""))
	} else {
		parsedTemplate, err := template.ParseFiles("templates/index.html")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			log.Fatal(err)
		}
		if err := parsedTemplate.Execute(w, nil); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			log.Fatal(err)
		}
	}
}

func main() {
	// read env variables
	if err := loadConfigs(); err != nil {
		log.Fatal(err)
	}

	logFile, err := os.OpenFile(logPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		fmt.Printf("unable to open %s\n", logPath)
		log.Fatal(err)
	}
	defer logFile.Close()
	appLogger = log.New(logFile, "", 0)

	http.HandleFunc("/", handleIndex)
	if err := http.ListenAndServe(":"+port, nil); err != nil {
		log.Fatal(err)
	}
}
