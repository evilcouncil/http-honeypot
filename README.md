# HTTP-Honeypot
---

Collect HTTP requests and store as JSON data.

## Config options
``` ini
PORT what port to listen on, default 8080
LOGFILE where to log requests to, default /var/log/http-honeypot/http-honeypot.log
SENSOR_NAME what is the name of this honeypot, default http-honeypot

```
