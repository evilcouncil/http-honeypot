FROM golang:1.18

WORKDIR /app
RUN mkdir /app/templates
RUN mkdir /var/log/http-honeypot
COPY go.mod ./
COPY go.sum ./
COPY templates/ templates
RUN go mod download
COPY *.go ./
RUN go build -o http-honeypot

CMD ["/app/http-honeypot"]
