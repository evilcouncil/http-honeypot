package main

const (
	PORT                = "PORT"
	DEFAULT_PORT        = 8080
	LOGFILE             = "LOGFILE"
	DEFAULT_LOGFILE     = "/var/log/http-honeypot/http-honeypot.log"
	SENSOR_NAME         = "SENSOR_NAME"
	DEFAULT_SENSOR_NAME = "http-honeypot"
)
